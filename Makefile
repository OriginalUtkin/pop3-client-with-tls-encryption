all:
	g++ -Wall -Wextra -pedantic -lssl -lcrypto main.cpp header.h -o popcl

clean:
	rm -f *~
	rm -f popcl

tar: clean
	tar -cf xutkin00.tar Makefile main.cpp header.h README

rmtar:
	rm -f xutkin00.tar
