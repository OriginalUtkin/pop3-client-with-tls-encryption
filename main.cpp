/**
*
* ISA project
* Project name : Klient POP3 s podporou TLS
* Vedouci projektu : Libor Polcak
* Autor projektu : Utkin Kirill, xutkin00@stud.fit.vutbr.cz, 2017
*
**/

#include "header.h"


/**
* @brief funkce main. Zacina program a zavola vsechny potrebne funkci
* @param int argc - pocet vstupnich parametru
* @param char** argv - pole, obsahujici vsichny vstupne parametry programu
* @return: 0 - v pripade uspesneho ukonceni programu
*/
int main(int argc, char** argv)
{
    int sock; // socket descriptor
    argValues values; // vstupni parametry programu
    TLSparam connection; // parametry pro sifrovane spojeni

    parseArguments(argc, argv, &values);

    sock = createSocket(&values);

    if(values.T)
        setSSLConnection(sock, &values);
    else if(values.S)
        startTLS(sock,connection, &values);
    else
        communicateWithPOP3(sock,&connection, &values );

    return 0;
}


/**
* @brief parseArguments provadi parsovani vstupnich argumentu programu. Zavola se ve funkci main primo od zacatku behu programu
* @param cosnt int argCount - pocet vstupnich paramtru programu. Argument je konstantou a nemuze byt zmenen.
* @param char** argv - pole, obsahujici vsichni vstipni argumenty
* @param argValues* - pointer do struktury, obsahujici potrebne parametry pro cinnost programu
* @return void
*/
void parseArguments(const int argCount, char** arg, argValues* values ){

    if(argCount == 1){
        printError(100, "[ERROR] : Not enough arguments. Type -h for help message.");
    }

    // Zpracovani help argumentu
    if(argCount == 2){
        string helpTest = arg[1];
        if((helpTest.compare("-h") == 0) || (helpTest.compare("--h") == 0)){
            printHelp();
        }else{
            printError(100,"[ERROR] : Not enough arguments. Type -h for help message." );
        }
    }

    //Zpracovani vshech vstupnish argumentu
    for(int counter = 1 ; counter < argCount; counter++){

        string tmp = arg[counter]; // tmp promenna pro ulozeni char* argumentu do string

        if(counter == 1){
            values->server = arg[1];
            continue;
        }

        // Zpracovani argumentu -a
        else if(tmp.compare("-a") == 0){

            if (counter + 1 == argCount)
                printError(100, "[ERROR] : Missing argument value <auth_path> for argument -a.");

            else{
                values->authPath = arg[counter + 1];
                if((!checkFile(values->authPath)) || (!isReadable(values->authPath)))
                    printError(100, "[ERROR] : Konfiguration file doesn't exist or no permissions to read a file.");
                else{
                    if(!checkFileFormat(values->authPath))
                        printError(100, "[ERROR] : Konfiguration file has wrong file format.");
                    parseFile(values);
                }
            }
        }

        // Zpracovani argumentu -o
        else if(tmp.compare("-o") == 0){

            if (counter + 1 == argCount)
                printError(100, "[ERROR] : Missing argument value <outh_path> for argument -o.");

            else{
                values->outPath = arg[counter+1];

                if((!checkDirectory(values->outPath)) || (!isReadable(values->outPath)) || (!isWritable(values->outPath)))
                    printError(100, "[ERROR] : Output directory doesn't exist or no permission to read or write to directory.");
            }
        }

        // Zpracovani argumentu -C
        else if(tmp.compare("-C") == 0){

            if (counter + 1 == argCount) // Argument -C nedostal cestu do adresare, obsah. certificaty
                printError(100, "[ERROR] : Missing argument value <certaddr> for argument -C.");

            else{
                values->C_param = arg[counter+1];

                if((!checkDirectory(values->C_param))) // Pokud adresar neexistuje
                    printError(100, "[ERROR] : Certificate directory <certaddr> doesn't exist.");
            }
        }

        //Zpracovani argumentu -c
        else if(tmp.compare("-c") == 0){

            if (counter + 1 == argCount) // Argument -c nedostal cestu do certifikacniho souboru
                printError(100, "[ERROR] : Missing argument value <certfile> for argument -c.");

            else{
                values->c_param = arg[counter+1];

                if((!checkFile(values->c_param))) // Pokud adresar neexistuje
                    printError(100, "[ERROR] : Certificate file <certfile> doesn't exist.");
            }
        }

        // Zpracovani argumentu -n
        else if(tmp.compare("-n") == 0){
            values->n = true;
        }

        // Zpracovani argumentu -d
        else if(tmp.compare("-d") == 0){
            values->d = true;
        }

        // Zpracovani argumentu -T
        else if(tmp.compare("-T") == 0){
            values->T = true;
        }

        // Zpracovani argumentu -S
        else if(tmp.compare("-S") == 0){
            values->S = true;
        }

        else if(tmp.compare("-p") == 0){

            long int tmpPortValue = 0;

            values->portFlag = true;
            values->portArg = arg[counter + 1];

            if((tmpPortValue = checkPortFormat(values->portArg)) == -1)
                printError(100, "[ERROR] : Wrong port value <port>. Value should be greater than 1023 or equal to 110(for TLS) "
                                "or equal to 995(for TLS connection). Value should be a number.");
            else
                values->port = tmpPortValue;

        }

        // Zpracovani neznameho argumentu
        else{
            continue;
        }
    }

    if((values->authPath.empty()) || (values->outPath.empty()) || (values->server.empty())){ // Nebyl nastaven jeden z povinnych parametru
        printError(100, "[ERROR] : Missing required parameters. Type -h for help message.");
    }

    if(values->T && values->S){ // Kombinovani parametru -S a -T
        printError(100, "[ERROR] : Argument -S can't be combined with -T argument.");
    }

    if(!values->portFlag){
        values->port = setDefaultPort(values);
    }
}


/**
* @brief createSocket vytvari IPv4/IPv6 socket pro komunikace s serverem. V pripade uspesneho vytvoreni vrati
* descripptor vytvorenneho soketu. Jinak, zavola funkci printError s odpovidajicim kodem a chybovym hlasenim.
* @param argValues* values - pointer na strukturu, obsahujici vsichny potrebne parametry pro beh programu.
* @return int socket - descriptor vytvoreneho soketu
*/
int createSocket(argValues* values){

    int sock, connect_status, host_status;
    struct addrinfo hints,*re;

    bzero( &hints, sizeof( struct addrinfo ) );

    hints.ai_family = AF_UNSPEC; // Podpora nezavisle komunikace na typu adresy serveru
    hints.ai_socktype = SOCK_STREAM;

    if((host_status = getaddrinfo(values->server.c_str(), to_string(values->port).c_str(), &hints, &re)) != 0)
        printError(101, "[ERROR] : Server not found.");

    if ((sock = socket(re->ai_family, re->ai_socktype, re->ai_protocol)) < 0)
        printError(101, "[ERROR] : Socket can't be created.");

    if ((connect_status = connect(sock, re->ai_addr, re->ai_addrlen)) != 0)
       printError(101, "[ERROR] : Connection to the server failed.");

    return sock;
}


/**
 * @brief InitCTX  inicializace kontextu pro SSL spojeni
 * @return SSL_CTX* ctx - pointer na vytvoreny kontext
 * https://www.cs.utah.edu/~swalton/listings/articles/ssl_client.c
 */
SSL_CTX* InitCTX(){

    const SSL_METHOD *method;
    SSL_CTX *ctx;

    OpenSSL_add_all_algorithms();  // Otevreni cryptos a ostatnich algoritmu
    SSL_load_error_strings();   /* Bring in and register error messages */
    method = TLSv1_2_client_method();  // Vytvoreni metody pro TLS komunikaci
    ctx = SSL_CTX_new(method);   // Vytvoreni noveho kontextu
    if ( ctx == NULL )
    {
        printError(102, "[ERROR] : The creation of a new SSL_CTX object failed.");
    }
    return ctx;
}


/**
 * @brief startTLS zacina komunikaci s serverem bez vyuziti sifrovani, pak ale prepina na sifrovanou komunikaci
 * @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @param TLSparam connection - struktura, obsahujici vsichny potrebne parametry pro SSL spojeni(vzdy prazdna)
 * @param const argValues* values - pointer na strukturu, obsahujici vsichny vstupni argumenty
 * @return void
 */
void startTLS(const int sock, TLSparam connection,const argValues* values){

    string response;

    response = getServerResp(&connection, sock);

    if(response.find("+OK") ==  string::npos){ // Neuspesne pripojeni do serveru
        closeConnection(sock, &connection);
        printError(103, "[ERROR] : Connection to POP3 server failed.");
    }

    response = sendRequest(&connection, sock, "STLS\r\n");

    if(response.find("+OK") ==  string::npos){ // Neuspesne provadeni STLS prikazu
        response = sendRequest(&connection, sock, "QUIT\r\n"); // Ukonceni komunikaci
        closeConnection(sock, &connection);
        printError(103, "[ERROR] : POP3 server doesn't accept STLS command.");
    }else{
        setSSLConnection(sock, values);
    }
}


/**
* @brief communicateWithPOP3 zacina komunikaci s POP3 serverem. Zasila vsecnhy potrebne prikazy do serveru
* @param const int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
* @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
* @param const argValues* values - pointer na strukturu, obsahujici vsichny vstupni argumenty
* @return void
*/
void communicateWithPOP3(const int sock, TLSparam* connection, const argValues* values){

    string response;

    if(!values->S){

        response = getServerResp(connection, sock); // Zajisteni odpovedi ze serveru po pripojeni

        if(response.find("+OK") ==  string::npos){ // Neuspesne pripojeni do serveru
            closeConnection(sock,connection);
            printError(103, "[ERROR] : Connection to POP3 server failed.");
        }

    }

    //Zasilani autorizacniho prikazu USER <name>
    response = sendRequest(connection, sock, "USER " + values->login + "\r\n");
    if(response.find("+OK") ==  string::npos){
        closeConnection(sock,connection);
        printError(103, "[ERROR] : User doesn't exist on POP3 server.");
    }

    //Zasilani autorizacniho prikazu PASS <pass>
    response = sendRequest(connection, sock, "PASS " + values->password + "\r\n");

    if(response.find("+OK") ==  string::npos){ // Autorizace probehla neuspesne
        closeConnection(sock,connection);
        printError(103, "[ERROR] : User doesn't exist on POP3 server.");
    }

    string mailList = getMailList(connection, sock);

    int counter = getCounter(mailList); // Zajisteni poctu zprav

    if(!values->d){ // Stahovani zprav
        downloadMessages(connection, sock, counter, values->outPath, values); // Zpracovani vsech zprav
    }else{ // Mazani zprav
        deleteMessages(connection, values, sock, counter);
    }
}


/**
 * @brief setSSLConnection nastavuje vsichni potrebne promenne ze struktury TLSparam tak, aby bylo mozne
 * zahajit SSL komunikaci s serverem
 * @param const int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @param argValues* values - pointer na strukturu, obsahujici vsichny vstupni argumenty
 * @return void
 */
void setSSLConnection(const int sock, const argValues* values){

    TLSparam TLSc; // parametry potrebne pro TLS komunikaci
    SSL_library_init(); // Inicializace SSL
    TLSc.ctx = InitCTX(); // inicializace kontextu
    TLSc.ssl = SSL_new(TLSc.ctx); // inicializace SSL spojeni
    SSL_set_fd(TLSc.ssl, sock); // Navazovani ssl spojeni nad TCP soket
    int code;

    //Pokud neni uveden parametr ani -C ani -c je nutne pouzit defaultni slozku s certifikaty
    if( (!values->c_param.length()) && (!values->C_param.length()) ){
        code = SSL_CTX_set_default_verify_paths(TLSc.ctx);

        if (code != 1){
            closeConnection(sock, &TLSc);
            printError(102, "[ERROR] : Failed to load certificate file.");
        }

    // Je nastaven parametr -C. Bude pouzita slozka s certificaty, uvedena uzivatelem
    }else if((!values->c_param.length()) && (values->C_param.length())){
        code = SSL_CTX_load_verify_locations(TLSc.ctx,NULL, values->C_param.c_str() );

        if (code != 1){
            closeConnection(sock, &TLSc);
            printError(102, "[ERROR] : Failed to load certificate file.");
        }

    // Je nastaven parametr -c. Bude pouzit certifikacni soubor, uvedeny uzivatelem
    }else if((values->c_param.length()) && (!values->C_param.length())){
        code = SSL_CTX_load_verify_locations(TLSc.ctx,values->c_param.c_str(), NULL );

        if (code != 1){
            closeConnection(sock, &TLSc);
            printError(102, "[ERROR] : Failed to load certificate file.");
        }

    // Parametry -c a -C jsou nastaveny.
    }else{
        code = SSL_CTX_load_verify_locations(TLSc.ctx,values->c_param.c_str(), values->C_param.c_str() );

        if (code != 1){
            closeConnection(sock, &TLSc);
            printError(102, "[ERROR] : Failed to load certificate file.");
        }
    }

    if((code = SSL_connect(TLSc.ssl)) != 1 ){   // Pri vytvoreni SSL spojeni doslo k nejake chybe

        closeConnection(sock, &TLSc);
        printError(102, "[ERROR] : Failed to create SSL connection.");
    }else{

        TLSc.server_certificate = SSL_get_peer_certificate(TLSc.ssl);
        TLSc.verify_result = SSL_get_verify_result(TLSc.ssl);

        //kontrola, zda certifikaty od serveru jsou validni
        if(TLSc.verify_result == X509_V_OK && TLSc.server_certificate != NULL){

            communicateWithPOP3(sock, &TLSc, values);
        }

        // Certifikaty od serveru nejsou platne
        else{

            closeConnection(sock, &TLSc);
            printError(102, "[ERROR] : Cannot verify server identity' " + values->server+".");
        }
    }
}


/**
* @brief sendRequest vytvari request(pozadavek) do SSL serveru a dostava response(odpoved) ze serveru.
* V pripade ze je vyuzita SSL komunikace a TLS parametry nejsou prazdne(NULL hodnoty), pak request se
* zasila pres SSL-layer(sifrovana komunikace), jinak je pouzit obecny socket(nesifrovana komunikace).
* @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
* @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
* @param string request - request ve formatu POP3(RFC1939), ktery je nutne poslat do POP3 serveru
* @return string serverResp - odpoved, prijatou ze serveru
*/
string sendRequest(TLSparam* connection, int sock, string request){

    int code;

    if(connection->ssl != NULL){

        code = SSL_write(connection->ssl, request.c_str(), strlen(request.c_str()));

        if((code < 0) || (code == 0)){

            closeConnection(sock,connection);
            printError(103, "[ERROR] : Error while reading response from a POP3 server.");
        }
    }

    else{

        code = send(sock, request.c_str(), request.length(), 0);

        if(code == -1 || (unsigned)code < request.length()){
            closeConnection(sock,connection);
            printError(103, "[ERROR] : Error while sending request to a POP3 server.");
        }
    }

    string serverResp = getServerResp(connection, sock);

    return serverResp;
}


/**
 * @brief getServerResp zajistuje odpoved od serveru
 * @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
 * @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @return string response - prijata odpoved
 */
string getServerResp(TLSparam*connection,  int sock){

        char buf[1024] = {0};
        int bytes = 0;
        int totalRecv = 0;
        int counter = 0;
        bool rf = false;

        while(true){

            char lastSymb[1];

            if(connection->ssl != NULL) // Nacitani dat z SSL vrstvy
                bytes = SSL_read(connection->ssl,lastSymb,1);
            else{ // Nacitani dat primo ze soketu
                bytes = recv(sock, lastSymb, 1, 0);
            }

            if(lastSymb[0] == '\r')
                rf = true;
            if(rf && lastSymb[0] == '\n'){
                buf[counter++] = lastSymb[0];
                totalRecv += bytes;
                break;
            }

            buf[counter++] = lastSymb[0];
            totalRecv += bytes;
        }

        buf[totalRecv] = 0;
        string response = string(buf);
        memset(buf,0, sizeof(buf));

        return response;
}


/**
 * @brief getMailList zjiskava obsah POP3 serveru
 * @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
 * @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @return string result - seznam zprav na POP3 serveru
 */
string getMailList(TLSparam* connection , int sock){

    string listReq = "LIST\r\n";
    vector<char> mailList;
    int readFlag = 0;
    int receaved = 0;

    if(connection->ssl != NULL){ // SSL spojeni

        int writeFlag = SSL_write(connection->ssl, listReq.c_str(), strlen(listReq.c_str()));

        if(writeFlag < 0 || writeFlag == 0){
            closeConnection(sock, connection);
            printError(103, "[ERROR] : Error while sending request to a POP3 server.");
        }
    }else{ // Nesifrovane spojeni

        int writeFlag = send(sock, listReq.c_str(), listReq.length(), 0);

        if(writeFlag == -1 || (unsigned)writeFlag < listReq.length()){
            printError(103, "[ERROR] : Error while sending request to a POP3 server.");
            closeConnection(sock, connection);
        }
    }

    while(true){

        char lastSymb[1];

        if(connection->ssl != NULL){ // cteni ze SSL vrstvy

            readFlag = SSL_read(connection->ssl, lastSymb, 1);

            if(readFlag== 0 || readFlag < 0){
                printError(103, "[ERROR] : Error while reading response from a POP3 server.");
                closeConnection(sock, connection);
            }

        }else{ //Nesifrovane spojeni

            readFlag = recv(sock, lastSymb, 1,0);

            if(readFlag == -1){
                printError(103, "[ERROR] : Error while reading response from a POP3 server.");
                closeConnection(sock, connection);
            }
        }

        if(lastSymb[0] == '\n' && (mailList.at((mailList.size() - 1)) == '\r') && (mailList.at((mailList.size() - 2)) == '.')){
            mailList.push_back(lastSymb[0]);
            receaved += readFlag;
            break;
        }

        mailList.push_back(lastSymb[0]);
        receaved += readFlag;
    } // konec while

    mailList.push_back('\0');
    string result(mailList.begin(), mailList.end());
    result.erase(0,(result.find('\n'))+1);

    return result;
}


/**
 * @brief openMessage zjiskava obsah zpravy pro stazeni ze serveru
 * @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
 * @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @param int msgNumber - cislo zpravy pro stazeni
 * @return string result - obsah zpravy pro ulozeni do adresare
 */
string openMessage(TLSparam* connection, int sock, int msgNumber){

    vector<char> msg; // obsah mailu
    int readFlag = 0; // result provadeni operaci SSL_read nebo receaved
    int receaved = 0; // pocet prijatych bajtu

    string msgReq = "retr " + to_string(msgNumber)+"\r\n";
    int iter = 0;

    //Priznaky ukoncovani mailu
    bool firstCf = false;
    bool dot = false;

    if(connection->ssl){ // Zapis do SLL vrstvy

        int writeFlag = SSL_write(connection->ssl, msgReq.c_str(), strlen(msgReq.c_str()));

        if((writeFlag < 0) || (writeFlag == 0)){ //Pri zaslani prikazu doslo k chybe
            closeConnection(sock, connection);
            printError(103, "[ERROR] : Error while sending request to a POP3 server.");
        }
    }else{ // Zapis primo do soketu

        int writeFlag = send(sock, msgReq.c_str(), msgReq.length(), 0);

        if((writeFlag == -1) ){ //Pri zaslani prikazu doslo k chybe
            closeConnection(sock, connection);
            printError(103, "[ERROR] : Error while sending request to a POP3 server.");
        }
    }

   while(true){

        char lastSymb[1]; // posledni nacteny symbol

        if(connection->ssl){

            readFlag = SSL_read(connection->ssl,lastSymb,1);

            if(readFlag== 0 || readFlag < 0){

                printError(103, "[ERROR] : Error while reading response from a POP3 server.");
                closeConnection(sock, connection);
            }
        }else{

            readFlag = recv(sock, lastSymb, 1,0);

            if(readFlag == -1){

                printError(103, "[ERROR] : Error while reading response from a POP3 server.");
                closeConnection(sock,connection);
            }
        }


       if (lastSymb[0] == '\n' && !firstCf){
            firstCf = true;
            receaved +=readFlag;
            continue;
            iter++;
        }

        if(firstCf == false){
            iter++;
            continue;

        }else if(!dot){

            if(lastSymb[0] == '\n'){ // Nalezen konec zpravy
                if((((msg.at(msg.size()-1)) == '\r') && \
                  ((msg.at(msg.size()-2)) == '.') && \
                  ((msg.at(msg.size()-3)) == '\n') && \
                  ((msg.at(msg.size()-4)) == '\r'))) {

                    dot = true;
                    receaved += readFlag;
                    iter++;
                    break;
                }
                iter++;
                msg.push_back(lastSymb[0]);
                receaved += readFlag;
            }else{ // Pokracovani cteni
                iter++;
                msg.push_back(lastSymb[0]);
                receaved += readFlag;
            }
        }

        // Zprava je nactena, zkontroluj zda jeste nejaka data pro cteni
        else{
           break;
        }
    } // konec while

    string result(msg.begin(), msg.end());

    return result;
}


/**
 * @brief downloadMessages provadi parsovani mail souboru  a rozhodnuti o stazeni souboru
 * @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
 * @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @param int counter - pocet zprav pro stazeni
 * @param string folderName - cesta do adresare, kam ulozit stazene zpravy
 * @param const argValues* values - pointer na strukturu, obsahujici vsichny vstupni argumenty
 * @return void
 */
void downloadMessages(TLSparam* connection, int sock,  int counter, string folderName, const argValues* values){

    int downloadCounter = 0 ;
    string response;

    if(folderName.back() != '/')
        folderName += "/";

    for(int i = 1 ; i <= counter; ++i){
        string msg = openMessage(connection, sock,i);
        string name = getMsgName(msg);

        // Stahnout pouze nove zpravy ze serveru
        if(values->n){
            if(mailExist(name, folderName)){ // Zprava jiz existuje ve slozce
                continue;
            }else{ // nova zprava
                writeToFile(folderName + name, msg);
                downloadCounter += 1;
            }
        }

        // Stahnout vsichni zpravy ze serveru
        else{
            writeToFile(folderName + name, msg);
            downloadCounter += 1;
        }
    } // konec for

    if(!values->d){

        response = sendRequest(connection, sock, "QUIT\r\n"); // Ukonceni komunikaci
        closeConnection(sock, connection);

        if(values->n)
            cout << downloadCounter << " new message(s) was downloaded." << endl;
        else
            cout << downloadCounter << " message(s) was downloaded." << endl;
    }
}


/**
 * @brief deleteMessage provadi smazani zprav
 * @param TLSparam* connection - pointer na strukturu, obsahujici vsichny potrebne parametry pro SSL spojeni
 * @param const argValues* values - pointer na strukturu, obsahujici vsichny vstupni argumenty
 * @param int sock - descriptor soketu, vyuziteho pro komunikaci s serverem
 * @param counter - pocet zprav pro smazani
 * @return void
 */
void deleteMessages(TLSparam* connection, const argValues* values, int sock, const int counter){

    string response;

    if(!values->n){ // Pokud parametry '-d' a '-n' nejsou skombinovane

        for(int i = 1; i <= counter; ++i){

            response = sendRequest(connection, sock, "DELE " + to_string(i) + "\r\n");

            if(response.find("+OK") ==  string::npos){
                closeConnection(sock, connection);
                printError(103, "[ERROR] : Error while deliting messages on server.");
            }
        }

    }else{
        string mailList = getMailList(connection, sock);

        downloadMessages(connection, sock, counter, values->outPath, values);

        for(int i = 1; i <= counter; ++i){

            response = sendRequest(connection, sock, "DELE " + to_string(i) + "\r\n");

            if(response.find("+OK") ==  string::npos){
                closeConnection(sock, connection);
                printError(103, "[ERROR] : Error while deliting messages on server.");
            }
        }
    }

    response = sendRequest(connection, sock, "QUIT\r\n");
    closeConnection(sock, connection);
    cout << to_string(counter) + " message(s) was deleted." << endl;
}


/**
 * @brief writeToFile provadi zapis textu zpravy do souboru uvedeneho v fileName
 * @param fileName - nazev souboru(obsahujici cestu)
 * @param message - text zpravy
 * @return void
 */
void writeToFile(string fileName, string message){

    ofstream outFile(fileName);
    outFile << message;
    outFile.close();
}


/**
 * @brief getMsgName vytvari jmeno vysledneho mail souboru pro ulozeni do adresare
 * @param string msg - stazeny mail soubor
 * @return string name - nazev vysledneho souboru
 */
string getMsgName(string msg){

    string fromField = getField("From:", msg);
    string dateField = getField("Date:", msg);

    if((fromField.length() < 4 )|| (dateField.length() < 4))
        return getFromMail(getField("Message-ID:", msg));
    else
        return (getFromMail(fromField) + "-" + getDate(dateField));
}


/**
* @brief mailExist provadi kontrolu zda mail jiz existuje ve slozce.
* @param string fileName - nazev souboru
* @param string fldName - cesta do souboru
* @return TRUE v pripade ze soubor nalezen, jinak vrati FALSE
*/
bool mailExist(string fileName, string fldName){

    return (access((fldName + fileName ).c_str(), F_OK) != -1);
}


/**
 * @brief getField ziskava potrebny radek ze hlavicky zpravy
 * @param string findStr - nazev radku ze ktereho je nutne zjiskat potrebne info
 * @param string mail - zprava obsahujicic hlavicku
 * @return string finalSubString - text ktery obsahuje radek zpravy
 */
string getField(string findStr,string mail){

    for(unsigned int counter = 0; counter < mail.length(); ++counter){
        int endPos = mail.find('\n');
        string tmp = mail.substr(0, endPos);

        mail.erase(0, endPos+1);

        if((tmp.substr(0,findStr.length())).compare(findStr) == 0)
            return tmp;

        else
            continue;
    }
    return " ";
}


/**
 * @brief getSubject zjiskava info z radku Subject ve zprave
 * @param string mail - zprava
 * @return string subject - potrebne info
 */
string getSubject(string mail){

    mail = mail.erase(0, mail.find("Subject: "));
    int subStartPos = mail.find("Subject: ");
    int subEndPos = mail.find("To: ");
    string subject;

    for(int counter = subStartPos + 9; counter < subEndPos - 2; ++counter){
        subject.push_back(mail.at(counter));
    }

    return subject;
}


/**
* @brief getFromMail provadi ziskavani emailove adresy z polozky "From: "
* @param string name -polozka, obsahujici cely retezec z "From: "
* @return string finalName - emailova adresa ve tvaru <XXX@XXX.XXX>
*/
string getFromMail(string name){

    string finalName;

    for(unsigned int counter = name.find('<') + 1; counter < name.length(); ++counter){

            if(name.at(counter) == '>'){
                break;
            }else{
                finalName.push_back(name.at(counter));
            }
    }

    return finalName;
}


/**
 * @brief getDate zjiskava datum ze radku Date
 * @param string mailDate - obsah radku Date
 * @return string date - datum ve sprevnem tvaru
 */
string getDate(string mailDate){

    string date ;
    int lastSymb= 0;
    int lastNum = 0;

    for(unsigned int counter = 5 ; counter < mailDate.length(); ++counter){
        if(lastSymb < 2){
            if(mailDate.at(counter) == ':'){
                lastSymb += 1;
                date.push_back(mailDate.at(counter));
            }else{
                date.push_back(mailDate.at(counter));
            }
        }else{
            if(lastNum < 2){
                date.push_back(mailDate.at(counter));
                lastNum += 1;
            }else{
                break;
            }
        }
    }

    return date;
}


/**
* @brief printError vytiskne chybove hlaseni a ukonci program s odpovidajicim kodem
* @param int errCode - ukoncovaci kod
* @param string errMsg - chybove hlaseni
* @return void
*/
void printError(int errCode, string errMsg){
    cerr << errMsg << endl;
    exit(errCode);
}


/**
* @brief printHelp vytiskne napovedu
* @param void
* @return void
*/
void printHelp(){
    cout << helpMsg << endl;
    exit(0);
}


/**
 * @brief checkFile zajistuje zda soubor existuje v systemu
 * @param string fileName - nazev souboru (vc. cestu)
 * @return 1 pokud file nalezen, jinak vraci 0
 */
int checkFile(string fileName){
    ifstream file(fileName.c_str());

   return file.good();
}


/**
 * @brief checkFileFormat provadi kontrolu zda auth_file ma spravny tvar
 * @param string fileName - nazev authorizacnigo souboru (vc. cestu) pro kontrolu
 * @return int hodnotu 0 - pokud auth_file nema spravny tvar
 * @return int hodnotu 1 - pokud auth_file je vporadku
 */
int checkFileFormat(string fileName){

    ifstream file(fileName);
    string fileString;
    string userPattern, passPattern;

    getline(file, fileString);

    userPattern = fileString.substr(0, 11) ;

    if( fileString.length() != 0 && userPattern == "username = "){
        ;
    }else{
        return 0;
    }

    getline(file, fileString);

    passPattern = fileString.substr(0, 11) ;

    if( fileString.length() != 0 && passPattern == "password = ")
        ;
    else
        return 0;

    return 1;
}


/**
 * @brief parseFile zjiskavapotrebne informace ze authorizacniho souboru
 * @param argValues* values - pointer na strukturu, obsahujici vsichny vstupni argumenty
 * @return void
 */
void parseFile(argValues* values){

    ifstream file(values->authPath);
    string tmp;

    getline(file, tmp);
    values->login = tmp.substr(11,tmp.length() - 11);

    getline(file, tmp);
    values->password = tmp.substr(11, tmp.length() - 11);
}


/**
* @brief checkDirectory provadi kontrolu zda uvedena slozka existuje v systemu
* @param dirName - nazev (prip. cesta) slozky pro kontrolu
* @return 1 pokud slozka existuje, jinak vraci 0
*/
int checkDirectory(string dirName){

    DIR* directory;

    directory = opendir(dirName.c_str());

    if(directory == NULL)
        return 0;
    else
        return 1;
}


/**
* @brief closeConnection uzaveri vytvorene SSL spojeni (pokud existuje) a vytvoreny socket descriptor
* @param TLSparam* connection - struktura, obsahujici parametry pro SSL spojeni
* @param int sock - pouzity socket descriptor
* @return void
*
*/
void closeConnection(int sock, TLSparam* connection){

    if(connection ->ssl != NULL)
        SSL_free(connection->ssl);

    if(connection->ctx != NULL)
        SSL_CTX_free(connection->ctx);

    close(sock);
}


/**
* @brief getSize ziskava velikost emailu ktery soucasne zpracovava
* @param string& lst - pointer na seznam vsech mailu
* @return string value - velikost soucasneho mailu
*/
int getSize(string& lst){

    int endString = lst.find('\r');
    string sizeValue = lst.substr(1, endString);
    int value = stoi(sizeValue);

    int cutEnd = lst.find('\n');
    lst.erase(0,cutEnd + 1);

    return value;
}


/**
 * @brief getCounter zjiskava counter zprav pro stazeni/smazani
 * @param const string str - retezec, obsahujici pocet zprav
 * @return int value - pocet zprav
 */
int getCounter(const string str){

    return (count(str.begin(), str.end(), '\n') - 1 );
}


/**
 * @brief setDefaultPort nastavuje vhodnou hodnotu portu v pripade ze nebyl uveden parametr -p.
 * @param const argValues* values
 * @return long int hodnotu portu : 110 pro nesifrovanou komunikaci (prip. -S)
 * @return long int hodnotu portu : 995 pro SSL komunikaci
 */
long int setDefaultPort(const argValues* values){

    if(values->T)
        return 995;
    else
        return 110;
}


/**
 * @brief checkPortFormat kontroluje, zda port ma spravny tvar
 * @param string portString - retezec, obsahujici port
 * @return -1 pokud vstupni hodnota portu nema spravny tvar
 * @return long int portValue pokud port byl zadan spravne
 */
long int checkPortFormat(string portString){

    char* p_end;
    long int portValue = strtol(portString.c_str(), &p_end, 10);

    if((portValue < 0) || (portValue > 65535))
        return -1;

    else if(errno == ERANGE)
        return -1;

    else if(*p_end != '\0')
        return -1;

    else if(portValue < 1024 && portValue != 995 && portValue != 110)
        return -1;

    else
        return portValue;
}


/**
 * @brief isReadable provadi kontrolu zda jsou nastaveny prava na cteni souboru nebo slozky
 * @param const string path - cesta so douboru nebo slozky
 * @return bool true - pokud jsou prava pro cteni
 * @return bool false - prava pro cteni nejsou
 */
bool isReadable(const string path){

    if(access(path.c_str(), R_OK) == 0)
        return true;
    else
        return false;
}


/**
 * @brief isWritable provadi kontrolu zda jsou nastaveny prava na zapis do souboru nebo slozky
 * @param const string path - cesta so douboru nebo slozky
 * @return bool true - pokud jsou prava pro zapis
 * @return bool false - prava pro zapis nejsou
 */
bool isWritable(const string path){

    if(access(path.c_str(), W_OK) == 0)
        return true;
    else
        return false;
}
