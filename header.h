/**
*
* ISA project
* Project name : Klient POP3 s podporou TLS
* Vedouci projektu : Libor Polcak
* Autor projektu : Utkin Kirill, xutkin00@stud.fit.vutbr.cz, 2017
*
**/

#include <iostream>
#include <string>
#include <vector>
#include <cerrno>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <strings.h>
#include <fstream>
#include <algorithm>

#include <dirent.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <ctime>


using namespace std;

//struktura, obsahujici vsichny vstupne argumenty programu a jejich parametry
struct argValues{

    string portArg ; // port
    long int port;

    bool T = false; // argument -T (TLS spojeni)
    bool S = false; // argument -S (STLS)
    bool d = false; // argument -d (smazani zprav)
    bool n = false; // argument -n (stazeni pouze novych zprav)
    bool portFlag = false; // Flag argumentu -p

    string c_param = ""; // cesta do souboru z argumentu -c
    string C_param = ""; // cesta do adresare z argumentu -C

    string server = ""; // domenove jmsno nebo IP adresa serveru
    string authPath = ""; // cesta do authorizacneho souboru
    string outPath = ""; // cesta do output adresare

    string login = ""; // login z auth_file
    string password = ""; // password z auth_file
};


//struktura, obsahujici parametry pro vytvoreni SSL spojeni.
struct TLSparam{

    SSL_CTX* ctx = NULL; // pointer do SSL kontextu
    SSL* ssl = NULL; // pointer do SSL spojeni
    X509* server_certificate; //  pointer do certifikatu, ktery byl zaslan serverem
    long verify_result; // kod pro validace serveru
};


// Prototypy potrebnych funkci. Usporadany podle typu vracene hodnoty
void parseArguments(const int, char**, argValues*);
void communicateWithPOP3(const int sock, TLSparam* connection, const argValues* values);
void closeConnection(int sock, TLSparam* connection);
void printError(int, string);
void printHelp();
void parseFile(argValues* values);
void downloadMessages(TLSparam*, int, int, string, const argValues*);
void ShowCerts(SSL* ssl);
void formatMailList(string& lst);
void writeToFile(string, string);
void setSSLConnection(const int sock, const argValues* values);
void deleteMessages(TLSparam* connection, const argValues* values, int sock, const int counter);
void startTLS(const int sock, TLSparam connection,const argValues* values);

int createSocket(argValues*);
int checkFile(string);
int checkFileFormat(string fileName);
int checkDirectory(string dirName);
int getSize(string& lst);
int getCounter(const string str);
long int setDefaultPort(const argValues*);
long int checkPortFormat(string portString);

string sendRequest(TLSparam* connecton, int sock, string request);
string getServerResp(TLSparam* connection, int sock);
string getMailList(TLSparam* connection, int sock);
string openMessage(TLSparam* connection, int sock, int msgNumber);
string getField(string findStr ,string mail);
string getSubject(string mail);
string getMsgName(string msg);
string getFromMail(string name);
string getDate(string mailDate);
string getSynopsys(string msg, int msgSize);

bool mailExist(string fldName, string name);
bool isReadable(const string path);
bool isWritable(const string path);



const string helpMsg = "-------------------------------------------------------------------------\n"\
                 "Klient POP3 s podporou TLS\n\n"\
                 "Program umoznuje cteni elektronicke posty skrze protokol POP3 \n"\
                 "s moznosti vyuziti TLS sifrovani.\n"\
                 "-------------------------------------------------------------------------\n"\
                 "./popcl [-h|--h] server [-p port] [-T|-S [-c certfile] [-C certaddr]] [-d] \n"
                 "        [-n] -a auth_file -o out_dir\n"\
                 "-------------------------------------------------------------------------\n"\
                 "h                  vypis napovedy\n"\
                 "server             IPv4/IPv6/DNS adresa pop3 serveru, povinny\n"\
                 "p <port>           cislo portu pro kumunikaci, volitelny\n"\
                 "T                  zapina sifrovani cele komunikace (pop3s), volitelny\n"\
                 "S                  nesifrovane spojeni s prepnutim do sifrovaneho, volitelny\n"\
                 "c <certifile>      defenuje soubor certifile s certifikaty, volitelny\n"\
                 "C <certaddr>       urcuje adresar certaddr obsahujicic certifikaty, volitelny\n"\
                 "d                  smazani zprav ze serveru, volitelny\n"\
                 "n                  prace pouze s novymi zpravami, volitelny\n"\
                 "a <auth_file>      soubor obsahujici autorizcne udaje, povinny\n"\
                 "o <out_dir>        specifikuje vystupni adresar pro ulozeni zprav, povinny\n"\
             "-------------------------------------------------------------------------\n";
